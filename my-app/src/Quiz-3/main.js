import React from "react"
import {MovieProvider} from "./context"
import DaftarMovieList from "./list"
import DaftarMovieForm from "./form"

const MovieListEditor = () =>{
    return(
      <MovieProvider>
          <DaftarMovieList/>
          <br/>
          <br/>
          <DaftarMovieForm/>
      </MovieProvider>
    )
  }
  
  
  export default MovieListEditor
